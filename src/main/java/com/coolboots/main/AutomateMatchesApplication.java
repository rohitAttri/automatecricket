package com.coolboots.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("com.coolboots")
@EnableScheduling
public class AutomateMatchesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomateMatchesApplication.class, args);
	}

}
