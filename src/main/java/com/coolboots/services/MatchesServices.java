package com.coolboots.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.stereotype.Service;

import com.coolboots.dao.MatchesDao;
import com.coolboots.dto.CreateMatchDto;
import com.coolboots.dto.ScoreboardDto;
import com.coolboots.properties.PropertiesConfig;
import com.coolboots.util.CommonUtils;
import com.google.gson.Gson;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class MatchesServices {
	@Autowired
	MatchesDao matchesDao;

	@Autowired
	PropertiesConfig propConfig;

	public Map<String, Object> CreateMatches(CreateMatchDto createMatchDto) {
		Map<String, Object> response = new HashMap<>();
		response.put("status", 0);
		try {
			int matchId = 0;
			String date_start = null;
			String date_end = null;
			long timestamp_start = 0;
			long timestamp_end = 0;
			int minutes = createMatchDto.getAdd_time();

			matchId = matchesDao.getLatestMatchId();
			log.info("latest Match Id : {}", matchId);
			if (matchId != 0) {
				int matchCount = createMatchDto.getMatch_count();
				int matchesId[] = new int[matchCount];

				for (int i = 1; i <= matchCount; i++) {
					matchId = matchId + 1;
					matchesId[i - 1] = matchId;

					date_start = CommonUtils.addTimeToDate(minutes * i);
					date_end = CommonUtils.addTimeToDate((minutes * i) + 160);

					timestamp_start = CommonUtils.getTimeStamp(date_start);
					timestamp_end = CommonUtils.getTimeStamp(date_end);

					// matchesDao.createNewMatch(matchId);
					matchesDao.createNewMatch(matchId, date_start, date_end, timestamp_start, timestamp_end);
				}

				// generatePlayingXi(matchesId);
				// generateSportsTeamPlayerCreditsData();
				 insertPlayingXi(matchesId);
				 insertSportsTeamPlayerCreditsData(matchesId);
				// createScoreboard(matchesId);
				 log.info("dummy matches created");

				response.put("status", 1);
			} else {
				log.info("Error in getting latest match id");
			}
		} catch (Exception e) {
			log.error("Exception in MatchesServices CreateMatches() : {}", e.getMessage());
			e.printStackTrace();
		}
		return response;
	}


	// get data from property file and insert into playing xi
	private void insertPlayingXi(int[] matchesId) throws JSONException {
		JSONArray playersJsonData = new JSONArray(propConfig.getPlayersData());
		if (matchesId.length > 0 && playersJsonData.length() > 0) {
			// change string to json array
			for (int i = 0; i < matchesId.length; i++) {
				// System.out.println("playersJsonData :: " + playersJsonData.toString());
				matchesDao.batchInsertIntoPlayingXi(playersJsonData, matchesId[i]);
			}
		}
	}

	//get data from property file and insert into sports team player credits
	private void insertSportsTeamPlayerCreditsData(int[] matchesId) throws JSONException{
		JSONArray JsonData = new JSONArray(propConfig.getSportsTeamPlayerCreditData());
		if (matchesId.length > 0 && JsonData.length() > 0) {
			// change string to json array
			for (int i = 0; i < matchesId.length; i++) {
			//	System.out.println("JsonData :: " + JsonData.toString());
				matchesDao.batchInsertIntoSportsTeamPlayerCredits(JsonData, matchesId[i]);
			}
		}
	}


	// generate playing xi
	private void generatePlayingXi(int[] matchesId) throws JSONException {
		List<Map<String, Object>> players = matchesDao.getPlayingPlayers();
		System.out.println(players.toString());
		if (players != null && players.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			int teama = 0;
			int teamb = 0;
			System.out.println(players.size());
			for (Map<String, Object> item : players) {
				JSONObject itemJson = new JSONObject();
				itemJson.put("player_id", item.get("player_id"));
				itemJson.put("playing_role", item.get("playing_role"));
				if (Integer.parseInt(item.get("team_id").toString()) == 25 && teama < 11) {
					itemJson.put("playing11", 1);
					teama++;
				} else if (Integer.parseInt(item.get("team_id").toString()) == 17 && teamb < 11) {
					itemJson.put("playing11", 1);
					teamb++;
				} else {
					itemJson.put("playing11", 0);
				}
				jsonArray.put(itemJson);
			}
			System.out.println(jsonArray.toString());
		}
	}

	// generate sports team player credits data
	private void generateSportsTeamPlayerCreditsData() throws  JSONException{
		List<Map<String, Object>> data = matchesDao.getTeamPlayersCredit();
		System.out.println(data.toString());
		if (data != null && data.size() > 0) {
			JSONArray jsonArray = new JSONArray();
			System.out.println(data.size());
			for (Map<String, Object> item : data) {
				JSONObject itemJson = new JSONObject();
				itemJson.put("series_id", item.get("series_id"));
				itemJson.put("team_id", item.get("team_id"));
				itemJson.put("player_id", item.get("player_id"));
				itemJson.put("credits", item.get("credits"));
				itemJson.put("fantasy_points", item.get("fantasy_points"));
				itemJson.put("selected_by", item.get("selected_by"));
				itemJson.put("added_date", item.get("added_date"));
				jsonArray.put(itemJson);
			}
			System.out.println(jsonArray.toString());
		}
	}

	// get scoreCard
	public void createScoreboard(int[] matchesId) {
		try {
			String scoreCard = propConfig.getScoreCard();
			if (scoreCard != null) {
				for(int i=0;i<matchesId.length;i++){
					JSONObject jsonObject = new JSONObject(scoreCard);
					Map<String, Object> scorecard = new Gson().fromJson(jsonObject.toString(), HashMap.class);
					scorecard.replace("mid", matchesId[i]);
					matchesDao.insertIntoScoreCard(scorecard);
				}
				log.info("Score card generated in mongo");
			} else {
				log.info("No data in scoreCard : {}", scoreCard);
			}
		} catch (Exception e) {
			log.error("Exception in MatchesServices createScoreboard() : {}", e.getMessage());
			e.printStackTrace();
		}
	}

	public Map<String, Object> updateMatchStatus() {
		Map<String, Object> response = new HashMap<>();
		response.put("status", 0);
		response.put("result", "not updated");
		try {
			matchesDao.updateLiveStatus();
			matchesDao.updateCompletedStatus();
			log.info("status of matches updated successfully");
			response.put("status", 1);
			response.put("result", "updated");
		} catch (Exception e) {
			log.info("Exception in updating match status");
			log.error("Exception in MatchesServices updateMatchStatus() : {}", e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

	public Map<String, Object> getScoreboard() {
		Map<String, Object> response = new HashMap<>();
		response.put("status", 0);
		try {
			List<Integer> matchIds;
			matchIds = matchesDao.getLiveMatchIds();
			if(matchIds != null && matchIds.size()>0){
				createScoreboard(matchIds.stream().mapToInt(i->i).toArray());
				response.put("status", 1);
			}else{
				log.info("No live matches available");
			}
		} catch (Exception e) {
			log.info("Exception in inserting value in score_card");
			log.error("Exception in MatchesServices getScoreboard() : {}", e.getMessage());
			e.printStackTrace();
		}
		return response;
	}

//	GET SCORECARD API
//	public Map<String, Object> getScoreboard(ScoreboardDto request) {
//		Map<String, Object> response = new HashMap<>();
//		response.put("status", 0);
//		try {
//			String scoreCard = propConfig.getScoreCard();
//			if (scoreCard != null) {
//				JSONObject jsonObject = new JSONObject(scoreCard);
//				Map<String, Object> scorecard = new Gson().fromJson(jsonObject.toString(), HashMap.class);
//				scorecard.replace("mid", request.getMid());
//				response.put("my_contests", new ArrayList<>(1));
//				response.put("score_card", scorecard);
//				matchesDao.insertIntoScoreCard(scorecard);
//				response.put("status", 1);
//			} else {
//				log.info("No data in scoreCard : {}", scoreCard);
//				response.put("score_card", new HashMap<>(1));
//			}
//			response.put("status_note", "match started");
//		} catch (Exception e) {
//			log.error("Exception in MatchesServices getScoreboard() : {}", e.getMessage());
//			e.printStackTrace();
//		}
//		return response;
//	}
}
