package com.coolboots.controller;

import java.util.Map;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.coolboots.dto.CreateMatchDto;
import com.coolboots.dto.ScoreboardDto;
import com.coolboots.services.MatchesServices;

@RestController
@Log4j2
public class TestController {

	@Autowired
	MatchesServices matchesServices;

	@PostMapping(value = "create-matches")
	public Map<String, Object> getLiveMatches(@RequestBody CreateMatchDto createMatch) {
		return matchesServices.CreateMatches(createMatch);
	}

	@PostMapping(value = "update-status")
	public Map<String, Object> updateStatus() {
		log.info("updating status for matches");
		return matchesServices.updateMatchStatus();
	}

	@PostMapping(value = "insert-scoreboard")
	public Map<String, Object> saveScoreboard() {
		return matchesServices.getScoreboard();
	}

	//	@PostMapping(value = "get-score")
//	public Map<String, Object> getScore(@RequestBody ScoreboardDto request) {
//		return matchesServices.getScoreboard(request);
//	}
}
