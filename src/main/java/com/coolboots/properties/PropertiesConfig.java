package com.coolboots.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
@Setter
@Getter
@ToString
public class PropertiesConfig {

	@Value("${match.players.data}")
	private String playersData;

	@Value("${score.card.data}")
	private String scoreCard;

	@Value("${sports.team.player.credit.data}")
	private String sportsTeamPlayerCreditData;
}