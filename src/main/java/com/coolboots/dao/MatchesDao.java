package com.coolboots.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.configurationprocessor.json.JSONArray;

public interface MatchesDao {

	int getLatestMatchId();

	String createNewMatch(int matchId, String date_start, String date_end, long timestamp_start, long timestamp_end);
	// String createNewMatch(int matchId);

	List<Map<String, Object>> getPlayingPlayers();

	void batchInsertIntoPlayingXi(JSONArray jsonArray, int matchId);

	void updateLiveStatus();

	void updateCompletedStatus();

	List<Map<String, Object>> getTeamPlayersCredit();

	void batchInsertIntoSportsTeamPlayerCredits(JSONArray jsonData, int matchId);

    void insertIntoScoreCard(Map<String, Object> scoreCard);

	List<Integer> getLiveMatchIds();
}
