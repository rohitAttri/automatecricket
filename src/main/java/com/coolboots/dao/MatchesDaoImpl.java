package com.coolboots.dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.coolboots.util.QueryConstants;

import lombok.extern.log4j.Log4j2;

@Repository
@Log4j2
public class MatchesDaoImpl implements MatchesDao {

	ZoneOffset zone = ZoneOffset.of("+05:30");
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	MongoTemplate primaryMongo;

	@Override
	public int getLatestMatchId() {
		try {
			return jdbcTemplate.queryForObject(QueryConstants.GET_LATEST_MATCH_ID, Integer.class);
		} catch (Exception e) {
			log.error("Exception in MatchesDaoImpl getLatestMatchId() :{}", e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public String createNewMatch(int matchId, String date_start, String date_end, long timestamp_start,
			long timestamp_end) {
		try {
			jdbcTemplate.update(QueryConstants.INSERT_INTO_MATCHES, matchId, date_start, date_end, timestamp_start,
					timestamp_end);
			log.info("Data inserted into sports_matches for mid : {}", matchId);
		} catch (Exception e) {
			log.error("Exception in MatchesDaoImpl createNewMatches() :{}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Map<String, Object>> getPlayingPlayers() {
		try {
			return jdbcTemplate.queryForList(QueryConstants.GET_PLAYERS);
		} catch (Exception e) {
			log.error("Exception in MatchesDaoImpl getPlayingPlayers() :{}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Map<String, Object>> getTeamPlayersCredit() {
		try {
			//by efault mid used in query is 90000
			return jdbcTemplate.queryForList(QueryConstants.GET_TEAM_PLAYERS_CREDITS_DATA);
		} catch (Exception e) {
			log.error("Exception in MatchesDaoImpl getTeamPlayersCredit() :{}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	// batch insert hardcoded data into playing xi
	@Override
	public void batchInsertIntoPlayingXi(final JSONArray playerList, final int matchId) {
		try {
			jdbcTemplate.batchUpdate(QueryConstants.INSERT_INTO_PLAYINGXI, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					try {
						JSONObject player = playerList.getJSONObject(i);
						ps.setLong(1, player.getLong("player_id"));
						ps.setLong(2, matchId);
						ps.setString(3, player.getString("playing_role"));
						ps.setInt(4, player.getInt("playing11"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public int getBatchSize() {
					log.info("length of player list in batchInsertIntoPlayingXi :{}", playerList.length());
					log.info("Data inserted in to playingxi for mid:{}",matchId);
					return playerList.length();
//					return 0;
				}
			});
		} catch (Exception e) {
			log.error("Exception while inserting playingxi: {}", e.getMessage());
		}
	}

	// batch insert hardcoded data into sports team player credits
	@Override
	public void batchInsertIntoSportsTeamPlayerCredits(final JSONArray playerList, final int matchId) {
		try {
			jdbcTemplate.batchUpdate(QueryConstants.INSERT_INTO_SPORTS_TEAM_PLAYER_CREDITS, new BatchPreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					try {
						JSONObject player = playerList.getJSONObject(i);
						ps.setLong(1, player.getLong("series_id"));
						ps.setLong(2, matchId);
						ps.setLong(3, player.getLong("team_id"));
						ps.setLong(4, player.getLong("player_id"));
						ps.setDouble(5, player.getDouble("credits"));
						ps.setDouble(6, player.getDouble("fantasy_points"));
						ps.setDouble(7, player.getDouble("selected_by"));
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

				@Override
				public int getBatchSize() {
					log.info("length of player list in batchInsertIntoSportsTeamPlayerCredits :{}", playerList.length());
					log.info("Data inserted in to sports_team_player_credits for mid:{}",matchId);
					return playerList.length();
//					return 0;
				}
			});
		} catch (Exception e) {
			log.error("Exception while inserting playingxi: {}", e.getMessage());
		}
	}

	@Override
	public void insertIntoScoreCard(Map<String, Object> scorecard) {
		try {
			primaryMongo.insert(scorecard,"score_card");
		} catch (Exception e) {
			log.error("Exception while insertIntoScoreCard: {}", e.getMessage());
			log.info("Data inserted into mongo collection score_card");
			e.printStackTrace();
		}
	}



	@Override
	public void updateLiveStatus() {
		try {
			jdbcTemplate.update(QueryConstants.UPDATE_STATUS_FOR_LIVE);
			log.info("Status of scheduled match changed to live match");
		} catch (Exception e) {
			log.error("Exception while updateLiveStatus: {}", e.getMessage());
		}
	}

	@Override
	public void updateCompletedStatus() {
		try {
			jdbcTemplate.update(QueryConstants.UPDATE_STATUS_FOR_COMPLETED);
			log.info("Status of live match changed to completed match");
		} catch (Exception e) {
			log.error("Exception while updateCompletedStatus: {}", e.getMessage());
		}
	}

	// GET MATCH ID'S FOR SCORECARD
	@Override
	public List<Integer> getLiveMatchIds() {
		try {
			log.info("fetching match_id for scorecard");
			return jdbcTemplate.queryForList(QueryConstants.GET_MATCH_ID_OF_LIVE_MATCHES,Integer.class);
		} catch (Exception e) {
			log.error("Exception in MatchesDaoImpl getLiveMatchIds() :{}", e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
}
