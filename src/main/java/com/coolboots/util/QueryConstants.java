package com.coolboots.util;

public interface QueryConstants {
	String GET_LATEST_MATCH_ID = "SELECT match_id FROM sports_matches ORDER BY match_id DESC limit 1";
	String INSERT_INTO_MATCHES = "INSERT INTO `sports_matches`(`match_id`,`series_id`,`title`,`short_title`,`subtitle`,`format`,`status`,`pre_squad`,`verified`,`status_note`,`game_state`,`teama`,`teamb`,`priority`,`date_start`,`date_end`,`timestamp_start`,`timestamp_end`,`venue_name`,`venue_location`,`final_status`,`added_date`,`has_contest`,`sent_playing_xi`,`created`,`modified`)VALUES(?,'113485','India vs West Indies','INDIA vs WI','1st T20I','3','1','1','1','India won by 6 wickets (with 8 balls remaining)','1','25','17','0',?,?,?,?,'Rajiv Gandhi International Stadium, Uppal','Hyderabad','1',CURRENT_DATE,'0','0',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);";
	// String INSERT_INTO_MATCHES = "INSERT INTO
	// `sports_matches`(`match_id`,`series_id`,`title`,`short_title`,`subtitle`,`format`,`status`,`pre_squad`,`verified`,`status_note`,`game_state`,`teama`,`teamb`,`priority`,`date_start`,`date_end`,`timestamp_start`,`timestamp_end`,`venue_name`,`venue_location`,`final_status`,`added_date`,`has_contest`,`sent_playing_xi`,`created`,`modified`)VALUES(?,'113485','India
	// vs West Indies','INDIA vs WI','1st T20I','3','1','1','1','India won by 6
	// wickets (with 8 balls remaining)','0','25','17','0','2020-03-20
	// 19:00:00','2020-03-20 19:00:00','1575639000','1575682200','Rajiv Gandhi
	// International Stadium,
	// Uppal','Hyderabad','2',CURRENT_DATE,'0','0',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);";
	String GET_PLAYERS = "SELECT `player_id`, `playing_role`, `team_id` FROM `sports_players` WHERE team_id in (25,17)";
	String INSERT_INTO_PLAYINGXI = "INSERT INTO `sports_playing_xi`(`player_id`,`match_id`,`role`,`role_str`,`playing11`,`added_date`,`created`,`modified`)VALUES(?,?,?,'',?,CURRENT_DATE,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP);";
	String UPDATE_STATUS_FOR_LIVE = "update sports_matches set status = 3, final_status = 3, game_state=3 where date_start BETWEEN CURRENT_TIMESTAMP and ADDTIME(CURRENT_TIMESTAMP,\"500\") and Date(date_start) = CURRENT_DATE ";
	String UPDATE_STATUS_FOR_COMPLETED = "update sports_matches set status = 2, final_status = 2, game_state=2 where date_end < CURRENT_TIMESTAMP and status = 3 and Date(date_start) = CURRENT_DATE ";
    String GET_TEAM_PLAYERS_CREDITS_DATA = "SELECT `series_id`, `match_id`, `team_id`, `player_id`, `credits`, `fantasy_points`, `selected_by`, `added_date` FROM `sports_team_player_credits` WHERE team_id IN (17,25) and match_id = 90000";
	String INSERT_INTO_SPORTS_TEAM_PLAYER_CREDITS = "INSERT INTO `sports_team_player_credits`( `series_id`, `match_id`, `team_id`, `player_id`, `credits`, `fantasy_points`, `selected_by`, `added_date`, `created`, `modified` ) VALUES( ?, ?, ?, ?, ?, ?, ?, CURRENT_DATE, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP );" ;
    String GET_MATCH_ID_OF_LIVE_MATCHES = "SELECT `match_id` FROM `sports_matches` WHERE status = 3 and final_status = 3 and TIMESTAMPDIFF(MINUTE,date_start,NOW()) > 0 ";
}
