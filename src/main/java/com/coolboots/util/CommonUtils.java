package com.coolboots.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class CommonUtils {

	public static String checkDate(Date start, int timeToAdd) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(start.getTime());
		cal.add(Calendar.MINUTE, timeToAdd);
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(cal.getTime().getTime()));
	}

	public static String getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		TimeZone zone = TimeZone.getTimeZone("IST");
		cal.setTimeZone(zone);
		TimeZone.setDefault(zone);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(zone);
		return dateFormat.format(cal.getTime());
	}

	public static String addTimeToDate(int timeToAdd) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MINUTE, timeToAdd);
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Timestamp(cal.getTime().getTime()));
	}

	public static long getTimeStamp(String date) throws ParseException {
		Date parseDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
		long timestamp = parseDate.getTime();
		return timestamp;
	}

}
